package com.vazhasapp.shemajamebeli4

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.vazhasapp.shemajamebeli4.databinding.FragmentExpertModeBinding


class ExpertModeFragment : Fragment() {

    private var _binding: FragmentExpertModeBinding? = null
    private val binding get() = _binding!!

    var player1Wins = 0
    var player2Wins = 0

    private var activePlayer = 1

    private var player1 = ArrayList<Int>()
    private var player2 = ArrayList<Int>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentExpertModeBinding.inflate(inflater, container, false)
        return binding.root
    }

    fun bClick(){
        val selectedButton = view as Button

        var cellId = 0

        when(selectedButton!!.id){
            R.id.b1 -> cellId = 1
            R.id.b2 -> cellId = 2
            R.id.b3 -> cellId = 3
            R.id.b4 -> cellId = 4
            R.id.b5 -> cellId = 5
            R.id.b6 -> cellId = 6
            R.id.b7 -> cellId = 7
            R.id.b8 -> cellId = 8
            R.id.b9 -> cellId = 9
            R.id.b10 -> cellId = 10
            R.id.b11 -> cellId = 11
            R.id.b12 -> cellId = 12
            R.id.b13 -> cellId = 13
            R.id.b14 -> cellId = 14
            R.id.b15 -> cellId = 15
            R.id.b16 -> cellId = 16
            R.id.b17 -> cellId = 17
            R.id.b18 -> cellId = 18
            R.id.b19 -> cellId = 19
            R.id.b20 -> cellId = 20
            R.id.b21 -> cellId = 21
            R.id.b22 -> cellId = 22
            R.id.b23 -> cellId = 23
            R.id.b24 -> cellId = 24
            R.id.b25 -> cellId = 25
        }
        startGame(cellId, selectedButton)
    }

    private fun startGame(cellId:Int, buSelected: Button){

        activePlayer = if( activePlayer == 1 ){
            buSelected.setBackgroundResource(R.drawable.ic_x)
            buSelected.setBackgroundResource(R.color.light_blue)
            player1.add(cellId)
            2

        }else{
            buSelected.setBackgroundResource(R.drawable.ic_o)
            buSelected.setBackgroundResource(R.color.light_blue)
            player2.add(cellId)
            1
        }

        buSelected.isEnabled = false

        checkWinner()
    }

    private fun checkWinner() {

        var winner = -1

        when {
            // Row 1
            player1.contains(1) && player1.contains(2) && player1.contains(3) && player1.contains(4) && player1.contains(5)  -> winner = 1
            player2.contains(1) && player2.contains(2) && player2.contains(3) && player2.contains(4) && player2.contains(5) -> winner = 2
            // Row 2
            player1.contains(6) && player1.contains(7) && player1.contains(8) && player1.contains(9) && player1.contains(10)  -> winner = 1
            player2.contains(6) && player2.contains(7) && player2.contains(8) && player2.contains(9) && player2.contains(10) -> winner = 2
            // Row 3
            player1.contains(11) && player1.contains(12) && player1.contains(13) && player1.contains(14) && player1.contains(15)  -> winner = 1
            player2.contains(11) && player2.contains(12) && player2.contains(13) && player2.contains(14) && player2.contains(15) -> winner = 2
            // Row 4
            player1.contains(16) && player1.contains(17) && player1.contains(18) && player1.contains(19) && player1.contains(20)  -> winner = 1
            player2.contains(16) && player2.contains(17) && player2.contains(18) && player2.contains(19) && player2.contains(20) -> winner = 2
            // Row 5
            player1.contains(21) && player1.contains(17) && player1.contains(18) && player1.contains(19) && player1.contains(20)  -> winner = 1
            player2.contains(21) && player2.contains(17) && player2.contains(18) && player2.contains(19) && player2.contains(20) -> winner = 2

            player1.contains(1) && player1.contains(4) && player1.contains(7) -> winner = 1
            player1.contains(1) && player1.contains(4) && player1.contains(7) -> winner = 2
            // Col 2
            player1.contains(2) && player1.contains(5) && player1.contains(8) -> winner = 1
            player1.contains(2) && player1.contains(5) && player1.contains(8) -> winner = 2
            // Col 3
            player1.contains(3) && player1.contains(6) && player1.contains(9) -> winner = 1
            player1.contains(3) && player1.contains(6) && player1.contains(9) -> winner = 2
        }
        if (winner == 1) {
            player1Wins += 1
            Toast.makeText(context, "Player 1 Wins", Toast.LENGTH_LONG).show()

        } else if (winner == 2) {
            player2Wins += 1
            Toast.makeText(context, "Player 2 Wins", Toast.LENGTH_LONG).show()
        }
    }

}