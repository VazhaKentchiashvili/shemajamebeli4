package com.vazhasapp.shemajamebeli4

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.vazhasapp.shemajamebeli4.databinding.FragmentStandartGameBinding

class StandartGameFragment : Fragment() {

    private var _binding: FragmentStandartGameBinding? = null
    private val binding get() = _binding!!

    var player1Wins = 0
    var player2Wins = 0

    private var activePlayer = 1

    private var player1 = ArrayList<Int>()
    private var player2 = ArrayList<Int>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentStandartGameBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bClick()
    }


    private fun bClick(){
        val selectedButton: Button? = null

        var cellId = 0

        when(selectedButton?.id){
            R.id.b1 -> cellId = 1
            R.id.b2 -> cellId = 2
            R.id.b3 -> cellId = 3
            R.id.b4 -> cellId = 4
            R.id.b5 -> cellId = 5
            R.id.b6 -> cellId = 6
            R.id.b7 -> cellId = 7
            R.id.b8 -> cellId = 8
            R.id.b9 -> cellId = 9
        }
        if (selectedButton != null) {
            startGame(cellId, selectedButton)
        }
    }

    private fun startGame(cellId:Int, buSelected:Button){

        activePlayer = if( activePlayer == 1 ){
            buSelected.setBackgroundResource(R.drawable.ic_x)
            buSelected.setBackgroundResource(R.color.light_blue)
            player1.add(cellId)
            2

        }else{
            buSelected.setBackgroundResource(R.drawable.ic_o)
            buSelected.setBackgroundResource(R.color.light_blue)
            player2.add(cellId)
            1
        }

        buSelected.isEnabled = false

        checkWinner()
    }

   private fun checkWinner() {

        var winner = -1

       when {
           // Row 1
           player1.contains(1) && player1.contains(2) && player1.contains(3) -> winner = 1
           player2.contains(1) && player2.contains(2) && player2.contains(3) -> winner = 2
           // Row 2
           player1.contains(4) && player1.contains(5) && player1.contains(6) -> winner = 1
           player2.contains(4) && player2.contains(5) && player2.contains(6) -> winner = 2
           // Row 3
           player1.contains(7) && player1.contains(8) && player1.contains(9) -> winner = 1
           player2.contains(7) && player2.contains(8) && player2.contains(9) -> winner = 2
           // Col 1
           player1.contains(1) && player1.contains(4) && player1.contains(7) -> winner = 1
           player1.contains(1) && player1.contains(4) && player1.contains(7) -> winner = 2
           // Col 2
           player1.contains(2) && player1.contains(5) && player1.contains(8) -> winner = 1
           player1.contains(2) && player1.contains(5) && player1.contains(8) -> winner = 2
           // Col 3
           player1.contains(3) && player1.contains(6) && player1.contains(9) -> winner = 1
           player1.contains(3) && player1.contains(6) && player1.contains(9) -> winner = 2
       }
        if (winner == 1) {
            player1Wins += 1
            Toast.makeText(context, "Player 1 Wins", Toast.LENGTH_LONG).show()

        } else if (winner == 2) {
            player2Wins += 1
            Toast.makeText(context, "Player 2 Wins", Toast.LENGTH_LONG).show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}